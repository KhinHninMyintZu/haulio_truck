//
//  JobListTableViewCell.swift
//  TrackingTruck
//
//  Created by khinhninmyintzu on 18/06/2020.
//  Copyright © 2020 khinhninmyintzu. All rights reserved.
//

import UIKit

class JobListTableViewCell: UITableViewCell {

    @IBOutlet weak var JobNumberLbl: UILabel!
    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var JobNumberValue: UILabel!
    @IBOutlet weak var companyValue: UILabel!
    @IBOutlet weak var addressValue: UILabel!
    @IBOutlet weak var acceptBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
