//
//  JobListViewController.swift
//  TrackingTruck
//
//  Created by khinhninmyintzu on 17/06/2020.
//  Copyright © 2020 khinhninmyintzu. All rights reserved.
//

import UIKit
import GoogleSignIn
import Alamofire
import JGProgressHUD

class JobListTableViewController: UITableViewController {
    @IBOutlet var jobsTableView: UITableView!
    var jobArray = [Jobs]()
    var jobObj = Jobs()
    var userinfo = UserInformation()
    var token : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        token = UserDefaults.standard.string(forKey: Constants.TOKEN)
        self.setupJOBTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if token != nil{
            print("Token is valid")
            self.fetchData()
        }else{
            
            let storyboard: UIStoryboard = UIStoryboard(name: Constants.LOGIN, bundle: nil)
            let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: Constants.LOGINIDENTIFIER) as UIViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func didTapSignOut(_ sender: AnyObject) {
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            GIDSignIn.sharedInstance().signOut()
        }
        self.userinfo.setNil()
        let storyboard: UIStoryboard = UIStoryboard(name: Constants.LOGIN, bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: Constants.LOGINIDENTIFIER) as UIViewController
        self.present(vc, animated: true, completion: nil)
    }
    private func setupJOBTable(){
        self.jobsTableView.delegate = self
        self.jobsTableView.dataSource = self
    }
    
    func fetchData(){
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)
        
        DispatchQueue.main.async {
            AF.request(Constants.HaulioJOBList, method: .get).responseJSON { (response) in
                hud.dismiss()
                switch response.result {
                case .failure(let error):
                    print(error)
                case .success(let data):
                    self.jobArray.removeAll()
                    do {
                        
                        if let arraylist = data as? [[String: Any]] {
                            
                            for array in arraylist{
                                
                                if let idCode = array[Constants.ID] as? Int{
                                    self.jobObj!.id = idCode
                                }
                                if let jobidCode = array[Constants.JOBID] as? Int{
                                    self.jobObj!.jobid = jobidCode
                                }
                                if let priority = array[Constants.PRIORITY] as? Int{
                                    self.jobObj!.priority = priority
                                }
                                if let company = array[Constants.COMPANY] as? String{
                                    self.jobObj!.company = company
                                }
                                if let address = array[Constants.ADDRESS] as? String{
                                    self.jobObj!.address = address
                                }
                                if let geolocation = array[Constants.GEOLOCATION] as? AnyObject{
                                    self.jobObj!.geolocation = geolocation
                                }
                                let object = Jobs(defaultid: (self.jobObj?.id)!, jobid: (self.jobObj?.jobid)!, priority: (self.jobObj?.priority)!, company: (self.jobObj?.company)!, address: (self.jobObj?.address)!, geolocation: self.jobObj?.geolocation as AnyObject)
                                self.jobArray.append(object)
                            }
                            self.jobsTableView.reloadData()
                        }
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
}

extension JobListTableViewController{
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.jobArray.count > 0){
            return self.jobArray.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let job = self.jobArray[indexPath.row]
        let cell = Bundle.main.loadNibNamed(Constants.JOBLIST, owner: self, options: nil)?.first as? JobListTableViewCell
        cell?.JobNumberValue.text = String(job.jobid)
        cell?.companyValue.text = job.company
        cell?.addressValue.text = job.address
        self.registerAcceptTap(cell: cell!)
        return cell!
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        jobObj = jobArray[indexPath.row]
        performSegue(withIdentifier: Constants.SHOWDETAILIDENTIFIER, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destinationVC = segue.destination as? JobDetailViewController else {
            return
        }
        destinationVC.jobdetails = jobObj
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = Constants.CELLHEADER
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) // my custom colour
        label.adjustsFontSizeToFitWidth = true
        
        headerView.addSubview(label)
        headerView.backgroundColor = #colorLiteral(red: 0.8784313725, green: 0.8784313725, blue: 0.8784313725, alpha: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        headerView.addConstraint(NSLayoutConstraint(item: label, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: headerView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1.0, constant: 20))
        headerView.addConstraint(NSLayoutConstraint(item: label, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: headerView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 15))
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    private func registerAcceptTap(cell : JobListTableViewCell){
        cell.acceptBtn.addTarget(self, action: #selector(changeAccept(sender:)), for: .touchUpInside)
    }
    
    @objc func changeAccept(sender: UIButton?)
    {
        self.performSegue(withIdentifier: Constants.SHOWDETAILIDENTIFIER, sender: sender?.tag)
    }
}
