//
//  JobDetailViewController.swift
//  TrackingTruck
//
//  Created by khinhninmyintzu on 18/06/2020.
//  Copyright © 2020 khinhninmyintzu. All rights reserved.
//

import UIKit
import MapKit
import GoogleSignIn
import SDWebImage
import JGProgressHUD
import MapKit

class JobDetailViewController: UIViewController, UISearchResultsUpdating {
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var NameLbl: UILabel!
    @IBOutlet weak var jobidValue: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tblView: UITableView!
    private let locationManager = CLLocationManager()
    var jobdetails : Jobs!
    var lat : NSNumber = 0.0
    var long : NSNumber = 0.0
    var userinfo = UserInformation()
    var jobdetailcontroller : JobDetailViewController!
    var searchCompleter = MKLocalSearchCompleter()
    var searchResults = [MKLocalSearchCompletion]()
    var resultSearchController = UISearchController()
    var resultarray = [String]()
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchCompleter.delegate = self
        // 1
        searchController.searchResultsUpdater = self
        // 2
        searchController.obscuresBackgroundDuringPresentation = false
        // 3
        searchController.searchBar.placeholder = "Search"
        // 4
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController!.view.backgroundColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
        // 5
        navigationItem.searchController = searchController
        // 6
        definesPresentationContext = true
        searchController.searchBar.delegate = self
        
        tblView.delegate = self
        tblView.dataSource = self
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            return controller
        })()
        self.tblView.reloadData()
        
        let myurl = UserDefaults.standard.url(forKey: Constants.PROFILEIMG)as NSURL?
        let urlString: String = myurl?.absoluteString ?? "NOIMAGE"
        profileImg.sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: ""))
        self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2
        self.profileImg.clipsToBounds = true
        
        self.NameLbl.text = UserDefaults.standard.string(forKey: Constants.FULLNAME)
        jobidValue.text = String(jobdetails.jobid)
        
        if jobdetails.geolocation[Constants.LATITUDE] != nil{
            lat = jobdetails.geolocation[Constants.LATITUDE] as! NSNumber
        }
        
        if jobdetails.geolocation[Constants.LONGITUDE] != nil{
            long = jobdetails.geolocation[Constants.LONGITUDE] as! NSNumber
        }
        
        self.checkLocationServices()
        self.fetchLocationOnMap(titlename: jobdetails.company, latitude: lat, longitude: long, needSetRegion: true)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        jobdetailcontroller = JobDetailViewController()
    }
    
    @IBAction func didTapSignOut(_ sender: AnyObject) {
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            GIDSignIn.sharedInstance().signOut()
        }
        self.userinfo.setNil()
        let storyboard: UIStoryboard = UIStoryboard(name: Constants.LOGIN, bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: Constants.LOGINIDENTIFIER) as UIViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            checkLocationAuthorization()
        } else {
            // Show alert letting the user know they have to turn this on.
            let alert = UIAlertController(title: "Did you turn on locaiton?", message: "It's recommended  to turn on  your location before continuing.", preferredStyle: .alert)
            //            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
            //            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
        case .denied: // Show alert telling users how to turn on permissions
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            mapView.showsUserLocation = true
        case .restricted: // Show an alert letting them know what’s up
            break
        case .authorizedAlways:
            break
        @unknown default:
            break
        }
    }
    
    func fetchLocationOnMap(titlename: String,latitude: NSNumber, longitude: NSNumber, needSetRegion: Bool) {
        let annotations = MKPointAnnotation()
        annotations.title = titlename
        annotations.subtitle = "Job Location"
        annotations.coordinate = CLLocationCoordinate2D(latitude:
            CLLocationDegrees(truncating: latitude), longitude: CLLocationDegrees(truncating: longitude))
        mapView.addAnnotation(annotations)
        
        if needSetRegion {
            let center = CLLocationCoordinate2D(latitude: CLLocationDegrees(truncating: latitude), longitude: CLLocationDegrees(truncating: longitude))
            let coordinateSpan = MKCoordinateSpan(latitudeDelta:  CLLocationDegrees(truncating: latitude), longitudeDelta: CLLocationDegrees(truncating: longitude))
            let mRegion = MKCoordinateRegion(center: center, span: coordinateSpan)
            self.mapView.setRegion(mRegion, animated: true)
        }
    }
    
    func hideTableView(){
        //searchController.isActive = false
        self.tblView.isHidden = true
        searchResults.removeAll()
        self.tblView.reloadData()
    }
}


extension JobDetailViewController: UISearchBarDelegate{
    func searchBar (_ searchBar: UISearchBar, textDidChange searchText: String) {
        if  searchText.count == 0 {
            searchResults.removeAll()
            self.tblView.reloadData()
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        searchCompleter.filterType = .locationsOnly
        searchCompleter.queryFragment = searchController.searchBar.text!
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        hideTableView()
    }
    
    func getLocation(from address: String, completion: @escaping (_ location: CLLocationCoordinate2D?)-> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) { (placemarks, error) in
            guard let placemarks = placemarks,
                let location = placemarks.first?.location?.coordinate else {
                    completion(nil)
                    return
            }
            completion(location)
        }
    }
    
}

extension JobDetailViewController: MKLocalSearchCompleterDelegate {
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchResults = completer.results
        self.tblView.isHidden = false
        self.tblView.reloadData()
    }
    
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        // handle error
        print("Error : \(error.localizedDescription)")
    }
}

extension JobDetailViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let searchResult = searchResults[indexPath.row]
        let strTitle = searchResult.title
        let strSubTitle = searchResult.subtitle
        cell.textLabel?.text = strTitle
        cell.detailTextLabel?.text = strSubTitle
        tableView.separatorColor = UIColor.lightGray
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let completion = searchResults[indexPath.row]
        let searchRequest = MKLocalSearch.Request(completion: completion)
        let search = MKLocalSearch(request: searchRequest)
        search.start { (response, error) in
            let coordinate = response?.mapItems[0].placemark.coordinate
            let latBySelect = (coordinate?.latitude)! as NSNumber
            let longBySelect = (coordinate?.longitude)! as NSNumber
            
            self.fetchLocationOnMap(titlename: completion.title, latitude: latBySelect, longitude: longBySelect, needSetRegion: false)
            
            self.hideTableView()
        }
        searchController.searchBar.text = ""
    }
}

