//
//  UserInformation.swift
//  TrackingTruck
//
//  Created by khinhninmyintzu on 20/06/2020.
//  Copyright © 2020 khinhninmyintzu. All rights reserved.
//
import UIKit
import GoogleSignIn

class UserInformation {
    func setUser(user: GIDGoogleUser){
        let userDefault = UserDefaults.standard
        //Saving Data in UserDefault
        userDefault.set(user.userID, forKey: Constants.USERID)
        userDefault.set(user.authentication.idToken, forKey: Constants.TOKEN)
        userDefault.set(user.profile.name, forKey: Constants.FULLNAME)
        userDefault.set(user.profile.givenName, forKey: Constants.GIVENNAME)
        userDefault.set(user.profile.familyName, forKey: Constants.FAMILYNAME)
        userDefault.set(user.profile.email, forKey: Constants.EMAIL)
        userDefault.set(user.profile.imageURL(withDimension: 200), forKey: Constants.PROFILEIMG)
    }
    func setNil(){
        UserDefaults.standard.set(nil, forKey: Constants.USERID)
        UserDefaults.standard.set(nil, forKey: Constants.TOKEN)
        UserDefaults.standard.set(nil, forKey: Constants.FULLNAME)
        UserDefaults.standard.set(nil, forKey: Constants.GIVENNAME)
        UserDefaults.standard.set(nil, forKey: Constants.FAMILYNAME)
        UserDefaults.standard.set(nil, forKey: Constants.EMAIL)
        UserDefaults.standard.set(nil, forKey: Constants.PROFILEIMG)
    }
}
