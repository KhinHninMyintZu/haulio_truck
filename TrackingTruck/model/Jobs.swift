//
//  Jobs.swift
//  TrackingTruck
//
//  Created by khinhninmyintzu on 17/06/2020.
//  Copyright © 2020 khinhninmyintzu. All rights reserved.
//

import UIKit

class Jobs{
    
    var id : Int!
    var jobid : Int!
    var priority : Int!
    var company : String!
    var address: String!
    var geolocation: AnyObject!
    
    required init?()
    {
        
    }
    
    required init(defaultid : Int, jobid : Int, priority : Int, company : String, address: String, geolocation: AnyObject){
        self.id = defaultid
        self.jobid = jobid
        self.priority = priority
        self.company = company
        self.address = address
        self.geolocation = geolocation
    }
    
    init(_ dictionary: [String: Any]) {
        self.id = dictionary[Constants.ID] as? Int
        self.jobid = dictionary[Constants.JOBID] as? Int
        self.priority = dictionary[Constants.PRIORITY] as? Int
        self.company = dictionary[Constants.COMPANY] as? String
        self.address = dictionary[Constants.ADDRESS] as? String
        self.geolocation = dictionary[Constants.GEOLOCATION] as? AnyObject
    }
}

