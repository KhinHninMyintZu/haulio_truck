//
//  LogInViewController.swift
//  TrackingTruck
//
//  Created by khinhninmyintzu on 18/06/2020.
//  Copyright © 2020 khinhninmyintzu. All rights reserved.
//

import UIKit
import GoogleSignIn

class LogInViewController: UIViewController ,GIDSignInDelegate{
    
    var profileImage : NSString!
    var userinfo = UserInformation()
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil{
            print("We got an Sing_In error: \(String(describing: error))")
        }else{
            userinfo.setUser(user: user)
            let storyboard: UIStoryboard = UIStoryboard(name: Constants.JOBLIST, bundle: nil)
            let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: Constants.JOBLISTIDENTIFIER) as UIViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
    }

}

