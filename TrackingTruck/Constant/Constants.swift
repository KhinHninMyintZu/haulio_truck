//
//  Constants.swift
//  TrackingTruck
//
//  Created by khinhninmyintzu on 20/06/2020.
//  Copyright © 2020 khinhninmyintzu. All rights reserved.
//

import Foundation

class Constants {
    //joblist json
    public static var HaulioJOBList = "https://haulio.io/joblist.json"
    
    //LogIn
    public static var LOGIN = "LogIn"
    public static var LOGINIDENTIFIER = "loginID"
    
    //loadNibName,JobList
    public static var JOBLIST = "JobList"
    public static var JOBLISTIDENTIFIER = "navigationID"
    
    //JobDetail
    public static var SHOWDETAILIDENTIFIER = "showDetail"
    public static var LATITUDE = "latitude"
    public static var LONGITUDE = "longitude"
    
    //TableViewCell Header
    public static var CELLHEADER = "Jobs Available"
    
    //User Information from Google SignIn
    public static var USERID = "userid"
    public static var TOKEN = "token"
    public static var FULLNAME = "fullname"
    public static var GIVENNAME = "givenname"
    public static var FAMILYNAME = "familyname"
    public static var EMAIL = "email"
    public static var PROFILEIMG = "profileimage"
    
    //JobList Information
    public static var ID = "id"
    public static var JOBID = "job-id"
    public static var PRIORITY = "priority"
    public static var COMPANY = "company"
    public static var ADDRESS = "address"
    public static var GEOLOCATION = "geolocation"
}
