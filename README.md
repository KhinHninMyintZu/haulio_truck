

# Welcome to Haulido Truck!

Hi! I'm Myint Zu and let me briefly explain about this assignment. In this assigned iOS project, I'm using MVC design pattern,Auto Layout and following highlights

1. Google Sign In for Sign In page
2. After signed In we show  job data from located  json file. Using  Alamofire HTTP networking library  we parse data to arrayObject and load to Table View.Create multi-screen apps with  Navigation Controller.
3. Using UIKit, MapKit, SDWebImage, JGProgressHUD for showing job detail Page.


To Run Haulido Truck
=
To run the Haulido Truck project, clone the repo, and run `pod install` from the  directory first.
  
Third party libraries that I am using  

```ruby

pod 'Google'

pod 'GoogleSignIn'

pod 'Alamofire'

pod 'SDWebImage'

pod 'JGProgressHUD'
```

Screen Shots
=
![Launch Image](https://i.ibb.co/JCpp8Kp/Launch.png) ![Launch Image](https://i.ibb.co/ZggV6MZ/Google-Sign-In.png)
![Launch Image](https://i.ibb.co/Hq0Sq5N/Google-Account.png) ![Launch Image](https://i.ibb.co/PT2qTCK/Google-Sign-In-Confirm.png)
![Launch Image](https://i.ibb.co/Nr5hKFM/JobList.png) ![Launch Image](https://i.ibb.co/4dz1q5t/Job-Detail.png)
![Launch Image](https://i.ibb.co/YQc7KPm/Search-At-Job-Detail.png) ![Launch Image](https://i.ibb.co/QMpm0GQ/Job-Detail-Search-Location.png)